// @ts-ignore
import "source-map-support/register";

import express from "express";
import webpack from "webpack";

// @ts-ignore
import opencage from "opencage-api-client";

import path from "path";
import fs from "fs";

import webpackConfig from "./webpack.config.js";
import { ILocationEntry, IOpenCageResult } from "./src/types/InputFormat.js";

const app = express();

// @ts-ignore
const compiler : webpack.Watching | webpack.Compiler = webpack(webpackConfig, function (error, stats) {
    // This is used to compile the webpack scripts into a bundle and deliver it to the user
    if (error || stats.hasErrors()) {
        // If there are errors report them
        if (error) {
            return console.log("Compiler Error: ", error);
        }
        if (stats.hasErrors()) {
            let info = stats.toJson();
            info.errors.forEach((error: string) => {
                console.error("Compilation: ", error);
            });
            return;
        }
    }

    // There are no errors, so display in the server logs that the file was compiled and is now available
    let count = stats.compilation.fileDependencies.size;
    let time = (stats.endTime - stats.startTime) / 1000.0;
    let filename = path.relative(path.join(__dirname, ".."), path.join(stats.compilation.outputOptions.path, stats.compilation.outputOptions.filename));
    console.log(`Compiled ${count} files in ${time}s to ${filename}`);
});

// Load the index file into memory and create the express listener that is required
fs.readFile("../www/index.html", (error, data) => {
    if (error) {
        console.log("Reading HTML Page: ", error);
    } else {
        console.log("Loading HTML Page");
        // For all requests that aren't listed elsewhere, serve the main page
        app.get("*", (req: express.Request, res: express.Response) => {
            console.log("Sending Index Page");
            res.send(data.toString());
        });
    }
});

// Allow serving of the bundle and other static files
app.use(express.static("./dist"));

app.use(express.json()); // Allows conversion of all inputs into json by default

let currentRows: ILocationEntry[] = [];
/**
 * Gets the location stored for a entry in currentRows and runs a query to get the current lat and long location, which is then stored in the currentRows entry
 * @param index The index to currentRows that is to be updated
 */
async function fetchLocation(index: number) {
    let result = await opencage.geocode({
        q: currentRows[index].location
    }) as IOpenCageResult;

    if (result.status.code === 200) {
        if (result.results.length > 0) {
            let place = result.results[0];
            console.log(`Got location for ${index} at (${place.geometry.lat}, ${place.geometry.lng})`);
            currentRows[index].lat = place.geometry.lat;
            currentRows[index].long = place.geometry.lng;
        }
    // Handle any errors that may occur
    } else if (result.status.code === 402) {
        console.log("Hit Limit");
    } else {
        console.log("Error with Location: ", result.status.message);
    }
}

/**
 * Called when the user wants to register a series of entries
 */
app.use("/register", (req: express.Request, res: express.Response) => {
    let rows: ILocationEntry[] = req.body.rows;

    let ids: number[] = [];

    // Call the fetchLocation function on all rows, and track what new ids have been added
    rows.forEach((row) => {
        ids.push(currentRows.length);
        currentRows.push(row);
        fetchLocation(currentRows.length - 1).catch((error) => {
            console.error("Error fetching: ", error);
        });
    });

    console.log("Registering");

    res.send(ids);
});

/**
 * Called when the user wants to view the status of a request for a particular job
 */
app.use("/status", (req: express.Request, res: express.Response) => {
    let id: number = req.body.id;

    if (id >= currentRows.length || id < 0) {
        // Make sure the id is within the required range
        res.send("Out of Range");
    } else {
        let row = currentRows[id];
        if (row.lat === undefined) {
            // The location data has still not yet been returned
            res.send("Pending");
        } else {
            // There is a location, so send it to the end user
            res.send({
                lat: row.lat,
                long: row.long
            });
        }
    }
});

app.listen(8080);