# Installing
To install and run the code, install the TypeScript compiler (`tsc`) and Yarn. Then install the required packages by running `yarn install` once all the packages are installed run `tsc --pretty true -p .` to compile all the required files into the build directory

# Running
In order to run the code, the `.env` file must be moved to the build directory. After this simply run `cd build && node server.js` then navigate to `http://localhost:8080` in order to access the webportal