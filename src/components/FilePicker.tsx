import React from "react";
import { FormGroup, FileInput, Button, ProgressBar } from "@blueprintjs/core";
import { ILocationEntry } from "../types/InputFormat";

interface IFilePickerProps {
    onSubmit: (c: ILocationEntry[]) => void; // Called when the user hits the submit button, the paramter will be the processed response
}

interface IFilePickerState {
    filename: string; // The filename of the selected file
    loaded: number; // The percentage of the file loaded into memory from disk
    content: string; // The content of the file loaded into memory
}

/**
 * Provides an extraction for a the file picker feature of the application
 */
export default class FilePicker extends React.Component<IFilePickerProps, IFilePickerState> {
    constructor(props: IFilePickerProps) {
        super(props);
        this.state = {
            filename: "Choose File...",
            loaded: 0,
            content: ""
        };
    }

    /**
     * Handles when the user submits the form for file submission. The event should be stopped to prevent a file being uploaded
     */
    handleFormSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        this.setState({ loaded: 0 });
        // Split the csv for each line
        let lines = this.state.content.split("\n");
        let info: ILocationEntry[] = [];
        lines.forEach((line, i) => {
            // Skip the first line
            if (i == 0) {
                return;
            }
            // For each line get the first and second column store in memory
            let s = line.split(",");
            if (s.length !== 2) {
                console.log("Error Parsing");
            }
            let row: ILocationEntry = {
                name: s[0],
                location: s[1]
            };
            info.push(row);
        });
        // The csv has been converted to memory, now send that information to the parent element
        this.props.onSubmit(info);
    }

    /**
     * Handles when the file input changes, this will also load the file from disk and store in memory until another file is picked
     */
    handleFileChange = (event: any) => {
        let file: File = event.target.files[0];
        this.setState({ filename: file.name });

        // Create the file reader and load the csv
        let fileReader = new FileReader();
        fileReader.onload = (e) => {
            // This is called intermediate of uploading, so if the file is still uploading report the percentage
            let percent = e.loaded / e.total;
            this.setState({ loaded: percent });
            if (percent === 1) {
                // The file has been successfully loaded
                let content = fileReader.result;
                this.setState({ content });
            }
        };
        fileReader.readAsText(file);
    }

    render() {
        return (
            <form onSubmit={ this.handleFormSubmit }>

                <FormGroup
                    label="Upload CSV File"
                    labelFor="file"
                    labelInfo="(required)">

                    <FileInput id="file" fill={ true } text={ this.state.filename } onInputChange={ this.handleFileChange } />

                    { this.state.loaded !== 0 &&
                        <>
                            <div style={{ padding: "0.3rem" }}></div>
                            <ProgressBar value={ this.state.loaded } stripes={ false } intent="primary" />
                        </>
                    }

                </FormGroup>

                <Button text="Upload" fill={ true } icon="arrow-right" intent="primary" type="submit" disabled={ this.state.loaded !== 1 } />

            </form>
        )
    }
}