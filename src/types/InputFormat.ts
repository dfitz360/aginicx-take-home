export interface ILocationEntry {
    name: string;
    location: string;
    id?: number;
    lat?: string;
    long?: string;
}

export interface IOpenCagePlace {
    formatted: string;
    geometry: {
        lat: string,
        lng: string
    };
    annotations: {
        timezone: {
            name: string;
        }
    }
}

export interface IOpenCageResult {
    status: {
        code: number,
        message: string
    };
    results: IOpenCagePlace[];
}