import React from "react";
import { Colors } from "@blueprintjs/core";
import FilePicker from "./components/FilePicker";
import { ILocationEntry } from "./types/InputFormat";
import axios from "axios";

interface IAppState {
    content: ILocationEntry[];
}

/**
 * The main app class
 */
export class App extends React.Component<{}, IAppState> {
    timer: NodeJS.Timeout; // Used for calling the updates

    constructor(props: {}) {
        super(props);
        this.state = {
            content: undefined
        };
    }

    componentDidMount() {
        // Set an interval to continuously update the content if it exists
        this.timer = setInterval(() => {
            if (this.state.content !== undefined) {
                this.state.content.forEach((row, i) => {
                    // For each entry check if it has information, then make a status request to the server
                    if (row.lat !== undefined) {
                        return;
                    }
                    if (row.id !== undefined) {
                        axios.post<string|{lat: string; long: string}>("/status", {
                            id: row.id
                        }).then((result) => {
                            if (result.status === 200) {
                                if (result.data === "Out of Range") {
                                    console.log("Should not be here");
                                } else if (result.data === "Pending") {
                                    return;
                                } else {
                                    // Successful content, store the new content in the state
                                    let content = result.data as { lat: string; long: string };
                                    this.state.content[i].lat = content.lat;
                                    this.state.content[i].long = content.long;
                                    this.setState({ content: this.state.content });
                                }
                            }
                        }).catch((error) => {
                            console.error("Error Communicating: ", error);
                        });
                    }
                });
            }
        }, 200);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    /**
     * Handles when the filepicker component is submitted
     */
    handleFileSubmit = (c: ILocationEntry[]) => {
        this.setState({ content: c });
        // Send a register request to the server
        axios.post<number[]>("/register", {
            rows: c
        }).then((result) => {
            if (result.status === 200) {
                console.log(result.data);
                // There was data, so add the job id to the existing list
                let oldLocations = this.state.content;
                let newLocations = oldLocations.map((loc, i) => {
                    return {
                        id: result.data[i],
                        ...loc
                    };
                });
                this.setState({ content: newLocations });
            } else {
                console.log("Error Occurred");
            }
        }).catch((error) => {
            console.error("Accessing Server: ", error);
        });
    }

    render() {

        let tableContent: JSX.Element = undefined;
        if (this.state.content !== undefined) {
            let rows: JSX.Element[] = [];
            this.state.content.forEach((row, i) => {
                rows.push((
                    <tr>
                        <td>{ row.id }</td>
                        <td>{ row.name }</td>
                        <td>{ row.lat }</td>
                        <td>{ row.long }</td>
                        <td>{ row.lat === undefined ? "Pending" : "Complete" }</td>
                    </tr>
                ));
            });
            tableContent = ((
                <table className="bp3-html-table bp3-html-table-striped bp3-interactive" style={{ width: "100%" }}>
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Name</td>
                            <td>Latitude</td>
                            <td>Longitude</td>
                            <td>Status</td>
                        </tr>
                    </thead>
                    <tbody>
                        { rows }
                    </tbody>
                </table>
            ));
        }

        return (
            <div style={{ backgroundColor: Colors.LIGHT_GRAY4, minHeight: "100vh", padding: "2rem" }}>
                <div style={{ maxWidth: "600px", marginLeft: "auto", marginRight: "auto" }}>
                    <h1 className="bp3-heading">CSV Location Parser</h1>
                    <FilePicker onSubmit={ this.handleFileSubmit } />

                    { tableContent !== undefined &&
                        <>{ tableContent }</>
                    }
                </div>
            </div>
        );
    }
}